Comentários da questão 7
alteração 1 - Reposicionamento da tag script no html
alteração 2 - Refatoração do ouvidor de evento para poder evitar o recarregamento automatico padrão da tag button
alteração 3 - Correção da função de conversão do valor de entrada no caso de graus em Fahrenheit

a. Qual foi o nível de dificuldade da implementação da questão do bloco A?
[ ] Muito Fácil
[x] Fácil
[ ] Médio
[ ] Difícil
[ ] Muito Difícil

b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?
[x] Muito Fácil
[ ] Fácil
[ ] Médio
[ ] Difícil
[ ] Muito Difícil
